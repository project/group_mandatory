<?php
/** @noinspection PhpUnnecessaryLocalVariableInspection */

namespace Drupal\group_mandatory;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityAccessControlHandlerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\CacheableTypes\CacheableBool;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Entity\GroupRelationshipType;
use Drupal\group\Entity\GroupRelationshipTypeInterface;
use Drupal\group\Entity\GroupTypeInterface;
use Drupal\group\Plugin\Group\Relation\GroupRelationTypeManagerInterface;
use Drupal\group_mandatory\Utility\GroupAndGroupRelationshipType;
use Drupal\route_override\Interfaces\RouteOverrideControllerBase;
use Drupal\route_override\Traits\OverrideEntityFormByBundleTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;

final class GroupMandatoryRouteOverrideController extends RouteOverrideControllerBase {

  use StringTranslationTrait;

  use OverrideEntityFormByBundleTrait;

  protected EntityTypeManagerInterface $entityTypeManager;

  protected EntityTypeBundleInfoInterface $entityTypeBundleInfo;

  protected GroupRelationTypeManagerInterface $groupRelationTypeManager;

  protected AccountProxyInterface $currentUser;

  public function __construct(EntityTypeManagerInterface $entityTypeManager, EntityTypeBundleInfoInterface $entityTypeBundleInfo, GroupRelationTypeManagerInterface $groupRelationTypeManager, AccountProxyInterface $currentUser) {
    $this->entityTypeManager = $entityTypeManager;
    $this->entityTypeBundleInfo = $entityTypeBundleInfo;
    $this->groupRelationTypeManager = $groupRelationTypeManager;
    $this->currentUser = $currentUser;
  }

  /**
   * Get the GroupRelationship EntityId, which is different in Group:2|3.
   */
  public function getGroupRelationshipEntityId(): string {
    return $this->entityTypeManager->hasDefinition('group_relationship')
      ? 'group_relationship' : 'group_content';
  }

  /**
   * Get the GroupRelationshipType EntityId, which is different in Group:2|3.
   */
  public function getGroupRelationshipTypeEntityId(): string {
    return $this->entityTypeManager->hasDefinition('group_relationship_type')
      ? 'group_relationship_type' : 'group_content_type';
  }

  protected function appliesToRouteOfEntityFormOfBundle(ConfigEntityInterface $bundleConfig, EntityTypeInterface $entityType, Route $route): CacheableBool {
    $isCreateForm = $this->isEntityCreateFormRoute($route, $entityType->id());

    $bundleName = $bundleConfig->id();
    // Config has string IDs.
    assert(is_string($bundleName));
    $entityTypeId = $bundleConfig->getEntityType()->getBundleOf();
    $hasGroupMandatory = $this->checkEntityBundleHasGroupMandatory($entityTypeId, $bundleName);
    // Apply to creating entities of bundles with group-mandatory configured.
    $applies = CacheableBool::and($hasGroupMandatory, $isCreateForm);
    return $applies;
  }

  protected function getListCacheabilityOfAppliesToRoute(EntityTypeInterface $entityType): CacheableDependencyInterface {
    return $this->getListCacheabilityOfEntityTypeBeingGroupRelationshipType();
  }

  public function appliesToRouteMatch(RouteMatchInterface $route_match, Request $request): CacheableBool {
    $entity = $this->extractEntityFromRouteMatchOfEntityForm($route_match);
    // Checking for create-/edit-/delete-form is not necessary here, as we only
    // subscribed to entity-create-form routes.
    $hasGroupMandatory = $this->checkEntityBundleHasGroupMandatory($entity->getEntityTypeId(), $entity->bundle());
    return $hasGroupMandatory;
  }

  protected function boolAccess(RouteMatchInterface $routeMatch, AccountInterface $account, Request $request = NULL): CacheableBool {
    // First we tried to check if we in-principle can create entities, even if
    // there is no group to do so. But the API was not friendly to that, any
    // access handler check needs a specific group and throws otherwise.
    // If and once we need it, we can copy the permission check from the access
    // handler.
    $entity = $this->extractEntityFromRouteMatchOfEntityForm($routeMatch);
    $groupAndGroupRelationshipTypes = $this->fetchGroupAndGroupRelationshipTypeItemsWithAccessToCreateGroupRelationship($entity->getEntityTypeId(), $entity->bundle(), $account);
    $cacheability = $this->fetchCacheabilityForGroupAndGroupRelationshipTypeItemsWithAccess($entity->getEntityTypeId(), $entity->bundle(), $account);
    return CacheableBool::create((bool) $groupAndGroupRelationshipTypes, $cacheability);
  }

  public function build(RouteMatchInterface $route_match, Request $request) {
    $entity = $this->extractEntityFromRouteMatchOfEntityForm($route_match);
    $bundleInfo = $this->entityTypeBundleInfo->getBundleInfo($entity->getEntityTypeId());
    $bundleLabel = $bundleInfo[$entity->bundle()]['label'];

    // No, this can not be injected with RequestValueResolvers.
    $account = $this->currentUser;
    $groupAndGroupRelationshipTypes = $this->fetchGroupAndGroupRelationshipTypeItemsWithAccessToCreateGroupRelationship($entity->getEntityTypeId(), $entity->bundle(), $account);
    $cacheability = $this->fetchCacheabilityForGroupAndGroupRelationshipTypeItemsWithAccess($entity->getEntityTypeId(), $entity->bundle(), $account);

    $build = [];
    (CacheableMetadata::createFromRenderArray($build))
      ->addCacheableDependency($cacheability)
      ->applyTo($build);
    $links = array_map(
      function(GroupAndGroupRelationshipType $groupAndGroupRelationshipType) {
        $group = $groupAndGroupRelationshipType->getGroup();
        $contentPluginId = $groupAndGroupRelationshipType->getGroupRelationshipType()->getPluginId();
        return [
          'group_label' => $group->label(),
          // @see \Drupal\group\Entity\Controller\GroupRelationshipController::addPage
          // @fixme
          'url' => Url::fromRoute("entity.{$this->getGroupRelationshipEntityId()}.create_form", [
            'group' => $group->id(),
            'plugin_id' => $contentPluginId,
          ])
        ];
      },
      $groupAndGroupRelationshipTypes
    );

    if ($links) {
      $build = [
        [
          // If we add a title here, it will become page title.
          // Do not duplicate page title here.
          // '#title' => $this->t('Create %bundle', ['%bundle' => $bundleLabel]),
          '#theme' => 'group_mandatory_list',
          '#links' => $links,
        ]
      ];
    }
    else {
      $build = ['#markup' => $this->t('You must be member of a group to do this.')];
    }
    return $build;
  }


  protected function fetchGroupAndGroupRelationshipTypeItemsWithAccessToCreateGroupRelationship(string $entityTypeId, string $bundleName, AccountInterface $account): array {
    // In the beginning, we tried iterating GroupRelationshipTypes and check
    // GroupRelationship createAccess for that, as that is the information needed in
    // Group module itself to check access. We saw that the AccessControlHandler
    // needs the specific group in $context, even if Group itself does not use
    // that (but OK, there is a permissions-per-group (bah!) module that
    // leverages that.
    // So we must iterate all groups which may be expensive.
    // @todo Upstream: Ask a way to bulk check access.
    // @todo Upstream: Request a cheaper shortcut
    // @see \Drupal\group\Entity\Access\GroupRelationshipAccessControlHandler::checkCreateAccess
    //
    // Then we saw that Group and EntityTypeWithBundle does not necessarily
    // determine GroupRelationshipType, so invented GroupAndGroupRelationshipType.
    //
    // @todo This is called twice, consider caching.

    $groupRelationshipTypes = $this->fetchMandatoryGroupRelationshipTypes($entityTypeId, $bundleName);
    $groups = $this->fetchGroupsForGroupRelationshipTypes($groupRelationshipTypes);
    $groupAndGroupRelationshipTypeItems = $this->getGroupAndGroupRelationshipTypeItemsWithAccess($groups, $groupRelationshipTypes, $account);

    return $groupAndGroupRelationshipTypeItems;
  }

  private function fetchGroupsForGroupRelationshipTypes(array $groupRelationshipTypes): array {
    /** @noinspection PhpUnhandledExceptionInspection */
    $groupStorage = $this->entityTypeManager->getStorage('group');

    $groupTypesByGroupRelationshipTypeId = array_map(
      fn(GroupRelationshipTypeInterface $groupRelationshipType) => $groupRelationshipType->getGroupType(),
      $groupRelationshipTypes
    );
    $groupTypeIds = array_map(
      fn(GroupTypeInterface $groupType) => $groupType->id(),
      $groupTypesByGroupRelationshipTypeId
    );
    $uniqueGroupTypeIds = array_unique($groupTypeIds);
    // We asked GroupStorage.
    /** @var GroupInterface[] $groups */
    $groups = $groupStorage->loadByProperties(['type' => $uniqueGroupTypeIds]);
    return $groups;
  }

  private function getGroupAndGroupRelationshipTypeItemsWithAccess(array $groups, array $groupRelationshipTypes, AccountInterface $account) {
    $groupRelationshipAccessControl = $this->entityTypeManager
      ->getAccessControlHandler($this->getGroupRelationshipEntityId());
    // We fetched an access handler.
    assert($groupRelationshipAccessControl instanceof EntityAccessControlHandlerInterface);

    /** @var array<string, array<int, GroupRelationshipTypeInterface>> $groupRelationshipTypesByGroupTypeId */
    $groupRelationshipTypesByGroupTypeId = array_reduce(
      $groupRelationshipTypes,
      function (array $result, GroupRelationshipTypeInterface $groupRelationshipType) {
        $result[$groupRelationshipType->getGroupTypeId()][] = $groupRelationshipType;
        return $result;
      },
      []
    );
    $groupAndGroupRelationshipTypeItems = [];
    foreach ($groups as $group) {
      $groupRelationshipTypesForGroup = $groupRelationshipTypesByGroupTypeId[$group->getGroupType()->id()];
      foreach ($groupRelationshipTypesForGroup as $groupRelationshipType) {
        // Using the access check for GroupRelationship does not give us the
        // desired result, ich checks for "create relation", not "create
        // entity".
        // @see \Drupal\group\Entity\Access\GroupRelationshipAccessControlHandler::checkCreateAccess
        // Do the same access check as the route access checks for the
        // entity.group_relationship.create_form route do.
        // @see \Drupal\group\Entity\GroupRelationship
        // @see \Drupal\group\Entity\Routing\GroupRelationshipRouteProvider::getCreateFormRoute
        // @see \Drupal\group\Access\GroupRelationshipCreateEntityAccessCheck::access
        // > "Retrieve the access handler from the plugin manager instead."

        /** @noinspection PhpUnhandledExceptionInspection */
        $entityCreateAccess = $this->groupRelationTypeManager
          ->getAccessControlHandler($groupRelationshipType->getPluginId())
          ->entityCreateAccess($group, $account);
        if ($entityCreateAccess) {
          $groupAndGroupRelationshipTypeItems[] = new GroupAndGroupRelationshipType($group, $groupRelationshipType);
        }
      }
    }
    return $groupAndGroupRelationshipTypeItems;
  }

  protected function fetchCacheabilityForGroupAndGroupRelationshipTypeItemsWithAccess(string $entityTypeId, string $bundleName, AccountInterface $account): CacheableDependencyInterface {
    $cacheability = new CacheableMetadata();
    // @todo Create and leverage CacheableArray.
    //   More precisely, if true, result is changed only after the last relevant
    //   GCT disappears (the any-item/list cacheability pattern).
    //   Leveraging the any-item/list cacheability pattern here would be too
    //   cumbersome, as we have no GRT array here, as we mapped GCT->GT in the
    //   other method.

    // Result is changed if any GCT is created or changed.
    $cacheability->addCacheableDependency($this->getListCacheabilityOfEntityTypeBeingGroupRelationshipType());

    // Result is changed if users group permissions change.
    // Vary by user's group permissions.
    // @see https://www.drupal.org/docs/contributed-modules/group/turning-off-caching-when-it-doesnt-make-sense
    $cacheability->addCacheContexts(['user.group_permissions']);
    // Invalidated when group memberships of current user change?
    // No, we don't need this, as of the cache context above.
    // @see https://www.drupal.org/project/group/issues/3090833

    // Of course, results change if groups are added or changed.
    $cacheability->addCacheableDependency($this->getListCacheabilityOfEntityTypeBeingGroup());

    return $cacheability;
  }

  private function checkGroupRelationshipTypeIsGroupMandatory(?GroupRelationshipTypeInterface $maybeGroupRelationshipType): CacheableBool {
    if ($maybeGroupRelationshipType) {
      $isGroupMandatory = (bool) $maybeGroupRelationshipType->getThirdPartySetting('group_mandatory', 'mandatory'. FALSE);
      if ($isGroupMandatory) {
        // Result is changed only when this GCT is changed.
        return CacheableBool::create(TRUE, $maybeGroupRelationshipType);
      }
    }
    // Any newly created GCT may change this.
    return CacheableBool::create(FALSE, $this->getListCacheabilityOfEntityTypeBeingGroupRelationshipType());
  }

  protected function checkEntityBundleHasGroupMandatory(string $entityTypeId, string $bundleName): CacheableBool {
    $mandatoryGroupRelationshipTypes = $this->fetchMandatoryGroupRelationshipTypes($entityTypeId, $bundleName);
    if ($mandatoryGroupRelationshipTypes) {
      // Result will change only after the last config is changed.
      $arbitraryGroupRelationshipType = reset($mandatoryGroupRelationshipTypes);
      return CacheableBool::create(TRUE, $arbitraryGroupRelationshipType);
    }
    else {
      // Any newly created GCT may change this.
      return CacheableBool::create(FALSE, $this->getListCacheabilityOfEntityTypeBeingGroupRelationshipType());
    }
  }

  private function getListCacheabilityOfEntityTypeBeingGroupRelationshipType(): CacheableDependencyInterface {
    return $this->getListCacheabilityOfEntityType($this->getGroupRelationshipTypeEntityId());
  }

  private function getListCacheabilityOfEntityTypeBeingGroup(): CacheableDependencyInterface {
    return $this->getListCacheabilityOfEntityType('group');
  }

  private function getListCacheabilityOfEntityType(string $entityId): CacheableDependencyInterface {
    /** @noinspection PhpUnhandledExceptionInspection */
    $groupRelationshipTypeEntityType = $this->entityTypeManager->getDefinition($entityId);
    assert($groupRelationshipTypeEntityType instanceof EntityTypeInterface);
    $cacheability = (new CacheableMetadata())
      ->setCacheTags($groupRelationshipTypeEntityType->getListCacheTags())
      ->setCacheContexts($groupRelationshipTypeEntityType->getListCacheContexts());
    return $cacheability;
  }

  /**
   * @return array<string, GroupRelationshipTypeInterface>
   */
  private function fetchGroupRelationshipTypesForEntityBundle(string $entityTypeId, string $bundleName): array {
    $groupRelationshipTypes = GroupRelationshipType::loadMultiple();
    $applicableGroupRelationshipTypes = array_filter($groupRelationshipTypes,
      function(GroupRelationshipTypeInterface $groupRelationshipType)
        use ($entityTypeId, $bundleName) {
        /** @var GroupRelationshipTypeInterface $groupRelationshipType */
        return $groupRelationshipType->getPlugin()
            ->getRelationType()->getEntityTypeId() === $entityTypeId
          && $groupRelationshipType->getPlugin()
            ->getRelationType()->getEntityBundle() === $bundleName;
      }
    );
    return $applicableGroupRelationshipTypes;
  }

  /**
   * @return array<string, GroupRelationshipTypeInterface>
   */
  private function fetchMandatoryGroupRelationshipTypes(string $entityTypeId, string $bundleName): array {
    $applicableGroupRelationshipTypes = $this->fetchGroupRelationshipTypesForEntityBundle($entityTypeId, $bundleName);
    $mandatoryGroupRelationshipTypes = array_filter($applicableGroupRelationshipTypes,
      fn(GroupRelationshipTypeInterface $groupRelationshipType) => $this->checkGroupRelationshipTypeIsGroupMandatory($groupRelationshipType)->value()
    );
    return $mandatoryGroupRelationshipTypes;
  }

}
