<?php

namespace Drupal\group_mandatory\Utility;

use Drupal\group\Entity\GroupInterface;
use Drupal\group\Entity\GroupRelationshipTypeInterface;

/**
 * Combines a Group and a compatible GroupRelationshipType.
 *
 * Although GroupType, EntityType, and Bundle usually determine a
 * GroupRelationshipType, this is not enforced and assuming this may break
 * anytime in the future.
 * Apart from that robustness, this appears to immensely simplify code.
 */
final class GroupAndGroupRelationshipType {

  protected GroupInterface $group;

  protected GroupRelationshipTypeInterface $groupRelationshipType;

  public function __construct(GroupInterface $group, GroupRelationshipTypeInterface $groupRelationshipType) {
    $this->group = $group;
    $this->groupRelationshipType = $groupRelationshipType;

    // According to the API, GCT's GroupType may be NULL, but that would break
    // this module in some other places, so care for that when it comes.
    $groupTypeId = $group->getGroupType()->id();
    $groupRelationshipTypeGroupTypeId = $groupRelationshipType->getPlugin()->getGroupTypeId();
    if ($groupRelationshipTypeGroupTypeId !== $groupTypeId) {
      throw new \LogicException("GroupRelationshipType '$groupRelationshipTypeGroupTypeId' does not match GroupType '$groupTypeId'.");
    }
  }

  /**
   * @return \Drupal\group\Entity\GroupInterface
   */
  public function getGroup(): GroupInterface {
    return $this->group;
  }

  /**
   * @return \Drupal\group\Entity\GroupRelationshipTypeInterface
   */
  public function getGroupRelationshipType(): GroupRelationshipTypeInterface {
    return $this->groupRelationshipType;
  }

}
